#include "../include/bmp.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>



struct __attribute__((packed))bmp_header{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

size_t get_padding(size_t const width){
  return (4 - (width * sizeof (struct pixel)) % 4) % 4;
}

struct bmp_header header_create(const struct image* img) {
  struct bmp_header header;
  header.bfType = TYPE;
  header.bfileSize = sizeof (struct bmp_header) + image_size(img) + img->height * img->width * get_padding(img->width);
  header.bfReserved = RES;
  header.bOffBits = sizeof(struct bmp_header);
  header.biSize = DIB_SIZE;
  header.biWidth = img->width;
  header.biHeight = img->height;
  header.biPlanes = PLANES;
  header.biBitCount = BPP;
  header.biCompression = COMP;
  header.biSizeImage = header.bfileSize - sizeof(struct bmp_header);
  header.biXPelsPerMeter = X_PPM;
  header.biYPelsPerMeter = Y_PPM;
  header.biClrUsed = NUM_COLORS;
  header.biClrImportant = IMP_COLORS;
  return header;
}

static enum read_status validate_header(struct bmp_header* header){
  if (header->bfType != TYPE) return READ_INVALID_TYPE;
  if (header->biBitCount != BPP) return READ_INVALID_BITS;
  if (header->bOffBits != sizeof(struct bmp_header)) return READ_INVALID_SIZE;
  return READ_OK;
}

static enum read_status read_header(struct bmp_header* header, FILE* in){
  if (!fread(header, sizeof(struct bmp_header), 1, in)) return READ_FILE_ERROR;
  return READ_OK;
}

static enum write_status write_header(struct bmp_header* header, FILE* out){
  if (!fwrite(header, sizeof (struct bmp_header), 1, out)) return WRITE_ERROR;
  return WRITE_OK;
}

static enum read_status read_data(FILE* in, uint32_t height, uint32_t width, struct image* img){
  for (uint32_t i = 0; i < height; i++) {
    if ((!fread(img->data + i * width, sizeof (struct pixel), width, in)) || fseek(in, (long) get_padding(width), SEEK_CUR)) {
      free(img->data);
      return READ_FILE_ERROR;
    }
  }
  return READ_OK;
}

static enum write_status write_data(FILE* out, const struct image* img){
  size_t buf = 0;
  size_t img_width = img->width;
  for (uint32_t i = 0; i < img->height; i++){
    size_t width = fwrite(img->data + i * img->width, sizeof(struct pixel),img_width, out);
    if (width != img_width) return WRITE_ERROR;
    if(!fwrite(&buf, get_padding(img_width), 1, out)) return WRITE_ERROR;
  }
  return WRITE_OK;
}

enum read_status from_bmp(FILE* in, struct image* img) {
  struct bmp_header header = {0};
  enum read_status error = read_header(&header, in);
  if (error) return error;
  error = validate_header(&header);
  if (error) return error;
  *img = image_create(header.biWidth, header.biHeight);
  if (fseek(in, header.bOffBits, SEEK_SET)){
    free(img->data);
    return READ_FILE_ERROR;
  }
  error = read_data(in, header.biHeight, header.biWidth, img);
  if (error) return error;
  return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img){
  struct bmp_header header = header_create(img);
  enum write_status error = write_header(&header, out);
  if (error) return error;
  error = write_data(out, img);
  if (error) return error;
  return WRITE_OK;
}
