#include "../include/inner_format.h"
#include <malloc.h>
#include <stdio.h>


struct image image_create(size_t width, size_t height) {
  struct image img = {0};
  img.width = width;
  img.height = height;
  img.data = malloc (width * height * sizeof (struct pixel));
  return img;
}

size_t image_size(struct image const* img) {
  return img->width * img->height * sizeof(struct pixel);
}
