#include "../include/file.h"
#include "../include/bmp.h"
#include "../include/rotation.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


static const char* file_errors[] = {
[FILE_WRONG_PATH] = "File with this path doesn't exist.\n",
[FILE_OPEN_ERROR] = "Error while opening file occured.\n",
[FILE_CLOSE_ERROR] = "Error while closing file occured.\n"
};

static const char* read_errors[] = {
  [READ_FILE_ERROR] = "Error while reading file occured.\n",
  [READ_INVALID_SIGNATURE] = "Invalid header signature.\n",
  [READ_INVALID_BITS] = "Invalid header bit count.\n",
  [READ_INVALID_HEADER] = "Invalid header.\n",
  [READ_ERROR] = "Error while reading bmp.\n",
  [READ_INVALID_SIZE] = "Invalid header size.\n",
  [READ_MALLOC_ERROR] = "Memory allocation error",
  [READ_INVALID_TYPE] = "Invalid header type.\n",
};

int main( int argc, char** argv ) {
  if (argc != 3) {
    fprintf(stderr, "The program requires 2 arguments, you specified %d\n", argc - 1);
    return 1;
  }

  FILE *in = NULL;
	FILE *out = NULL;
	struct image img = {0};
	struct image new_img;

  enum file_status file_error = file_open(&in, argv[1], "rb");
  if (file_error) {
		fprintf(stderr, "%s\n", file_errors[file_error]);
    return 1;
  }

  enum read_status read_error = from_bmp(in, &img);
  if (read_error) {
    free(img.data);
    file_close(in);
		fprintf(stderr, "%s\n", read_errors[read_error]);
    return 1;
  }

  file_error = file_close(in);
  if (file_error) {
    free(img.data);
		fprintf(stderr, "%s\n", "");
		fprintf(stderr, "%s\n", file_errors[file_error]);
    return 1;
  }

  rotate(&img, &new_img);
  free(img.data);

  file_error = file_open(&out, argv[2], "wb");
  if (file_error) {
    free(new_img.data);
		fprintf(stderr, "%s\n", file_errors[file_error]);
    return 1;
  }

  enum write_status write_error = to_bmp(out, &new_img);
  free(new_img.data);
  if (write_error) {
    file_close(out);
		fprintf(stderr, "%s\n", "Write error.\n");
    return 1;
  }

  file_error = file_close(out);
  if (file_error) {
		fprintf(stderr, "%s\n", file_errors[file_error]);
    return 1;
  }

  return 0;
}
