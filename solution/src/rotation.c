#include "../include/inner_format.h"
#include <stdint.h>
#include <stdlib.h>


static size_t get_rotated_by_90_pixel_index(struct image* const source, size_t x, size_t y){
  return (source->height - y - 1) * source->width + x;
}

void rotate( struct image* const source, struct image *result){
  *result = image_create(source->height, source->width);
  for(size_t i = 0; i < source->height; i++)
    for(size_t j = 0; j < source->width; j++) {
      result->data[j * result->width + i] = source->data[get_rotated_by_90_pixel_index(source, j, i)];
    }
}
