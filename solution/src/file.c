#include "../include/inner_format.h"
#include "../include/file.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>


enum file_status file_open (FILE **file, char const *file_path, char const *mode){
  if (!file_path) return FILE_WRONG_PATH;
  *file = fopen(file_path, mode);
  if (*file) return FILE_OK;
  return FILE_OPEN_ERROR;
}

enum file_status file_close (FILE *file){
    if (!file) return FILE_CLOSE_ERROR;
    fclose(file);
    return FILE_OK;
}
