#ifndef BMP_H
#define BMP_H

#include "file.h"
#include "inner_format.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define TYPE 19778
#define RES 0
#define PLANES 1
#define DIB_SIZE 40
#define COMP 0
#define BPP 24
#define X_PPM 0
#define Y_PPM 0
#define IMP_COLORS 0
#define NUM_COLORS 0


enum read_status from_bmp(FILE* in, struct image* img);

enum write_status to_bmp(FILE* out, struct image const* img);

#endif
