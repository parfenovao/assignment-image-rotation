#ifndef FILE_H
#define FILE_H

#include "inner_format.h"
#include <stdbool.h>
#include <stdio.h>

enum read_status  {
  READ_OK = 0,
  READ_FILE_ERROR,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_ERROR,
  READ_INVALID_SIZE,
  READ_MALLOC_ERROR,
  READ_INVALID_TYPE
};

enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
};

enum file_status  {
  FILE_OK = 0,
  FILE_WRONG_PATH,
  FILE_OPEN_ERROR,
  FILE_CLOSE_ERROR
};

enum file_status file_open (FILE **file, char const *file_path, char const *mode);

enum file_status file_close (FILE *file);

#endif
