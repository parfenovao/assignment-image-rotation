#ifndef INNER_FORMAT_H
#define INNER_FORMAT_H

#include <stdint.h>
#include <stdio.h>

struct image {
  size_t width, height;
  struct pixel* data;
};

struct __attribute__((packed))pixel {
  uint8_t b, g, r;
};

struct image image_create(size_t width, size_t height);

size_t image_size(struct image const* img);

#endif
