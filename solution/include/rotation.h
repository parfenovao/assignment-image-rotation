#ifndef ROTATION_H
#define ROTATION_H

#include "inner_format.h"

void rotate( struct image* const source, struct image *result);

#endif
